import java.util.Scanner;

public class Main2 {

  private static final String INPUT_NUMBER_R = "Введите натуральное число r: ";
  private static final String INPUT_NUMBER_M = "Введите натуральное число m: ";
  private static final String RESULT_MESSAGE = "Результат: ";

  public static int fact(int num) {
    return (num == 1) ? 1 : num * fact(num - 1);
  }

  public static int f(int m, int r) {
    return fact(m) / fact(r) * (m - r);
  }

  public static void main(String[] args) {
    System.out.print(INPUT_NUMBER_R);
    Scanner scan = new Scanner(System.in);
    int m = scan.nextInt();
    System.out.print(INPUT_NUMBER_M);
    int r = scan.nextInt();
    if (r <= m) {
      System.out.print(RESULT_MESSAGE + f(m, r) + "\r\n");
    } else {
      System.out.print("r > m\r\n");
    }
  }
}
