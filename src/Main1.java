public class Main1 {

  private static final String TWO_SEVEN = "TwoSeven\r\n";
  private static final String TWO = "Two\r\n";
  private static final String SEVEN = "Seven\r\n";

  public static void main(String[] args) {
    for (int i = 1; i <= 100; i++) {
      if ((i % 2 == 0) & (i % 7 == 0)) {
        System.out.print(TWO_SEVEN);
      } else {
        if (i % 2 == 0) {
          System.out.print(TWO);
        } else if (i % 7 == 0) {
          System.out.print(SEVEN);
        } else {
          System.out.print(i + "\r\n");
        }
      }
    }
  }
}
