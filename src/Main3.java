import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main3 {

  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.print("Введите литературный текст: ");
    String s = scan.nextLine();
    System.out.print("Результат: " + Arrays.toString(
        (Arrays.stream(s.toLowerCase().split(" ")).collect(Collectors.groupingBy(f -> f, Collectors.counting()))).entrySet().stream()
            .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                (oldValue, newValue) -> oldValue, LinkedHashMap::new)).entrySet().toArray()
        )+"\r\n"
    );
  }
}
